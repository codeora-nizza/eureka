

```
./gradlew build && java -jar -Dspring.profiles.active=dev build/libs/eureka-0.0.1-SNAPSHOT.jar
```

Docker commands

```aidl
$ ./gradlew build
...
$ docker build -t nizza-eureka-service .
...
$ docker container run -p 8761:8761 nizza-eureka-service:latest 
...
```